#+TITLE: git-yaml

* NAME

git-yaml is a tool that you should not use

* SYNOPSIS

git-yaml

* DESCRIPTION

goes to the root of your git repo, opens the yaml file in vim.

When you close vim, it gives you an opportunity to abort and then adds the
yaml file makes a commit with 'fix yaml' as the message and does git push

#+TITLE: Git tools

Put the bin of this repo in your path.

This works to allow new subcommands =yaml= and =ignore= because When we do
=git <subcommand>=, git will try normal stuff but then it will look if there is
a command =git-subcommand= available in the PATH.

See 'man git-yaml' and 'man git-ignore' for more info.
